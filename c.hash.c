//----------------------------------------------------------------------------------
//		c.hash.c
//

//----------------------------------------------------------------------------------
//	crc
//
i4 hash_CRC4= 0xEDB88320;
i4 hash_TBL4[0x100];
i4 crc4table() {	i4 a, v;																//	crc-32 table
					for(a= 1, v= 0; a <= 0x800; a++) {										//	one loop & two compares (0x100 * 0x008): limit & 8 bits
						v= (v >>1) ^(hash_CRC4 &(((v &1) ^1) -1));							//	
						if((a &7) == 0) { hash_TBL4[((a -1) >>3)]= v; v= (a >>3); }	}	}	//	
/*i4 crc4table() {
	for(a= 0; a < 0x100; a++) {																//	two loops
		for(b= 0, v= a; b < 0x008; b++) v= (v >>1) ^(hash_CRC4 &(((v &1) ^1) -1));
		hash_TBL4[a]= v;	}	}*/
i4 crc4(i4 c, i1 *b, sz0s size) {				i4 crc= ~(c);	i1 *bF= b +size;			//	crc-32
									while(b < bF) crc= (crc >>8) ^hash_TBL4[(crc &0x000000FF) ^(i4)(*(b++))];	return ~(crc);	}
//i4 crc4str(i1 *s) {		if(s == NULL) return 0;	return crc4(0, s, strlen(s));	}
//----------------------------------------------------------------------------------

