//----------------------------------------------------------------------------------
//		c.dump.c
//

//----------------------------------------------------------------------------------
//	nice (mirror) dump
//
#ifndef	mirrdump_indent
	#define mirrdump_indent "\t\t\t\t" 			//	starting indention
	#else
	#endif
void mirrdump(i1 *buf, sz0s sz, sz0s p) {		//	buffer, offset & size
	i1 buf1;
	sz0s i; for(i= p +(sz -1); i >= p; i--)	{
		buf1= buf[i];
		if(buf1 > 0x20 && buf1 < 0x7F)	{ fprintf(stderr, " %02X", buf1); }
		else							{ fprintf(stderr, " %02X", buf1); }
		if(i %0x100 == 0)				{ fprintf(stderr, " <=|== %016X""\n", i); }
		else	{	if(i %0x40 == 0)	{ fprintf(stderr, " <-|-- %016X""\n", i); }
			else {	if(i %0x10 == 0)	{ fprintf(stderr, "   |   %016X""\n", i); }
				else {}	}	}
		}										//	end of "from big to small" (small enginan)
	for(i= p; i <= p +(sz -1); i++)		{		//	start of "from small to big" (big endian)
		buf1= buf[i];
		if(i %0x100 == 0)				{ fprintf(stderr, "\n"mirrdump_indent"%016X ==|=> ", i); }
		else	{	if(i %0x40 == 0)	{ fprintf(stderr, "\n"mirrdump_indent"%016X --|-> ", i); }
			else {	if(i %0x10 == 0)	{ fprintf(stderr, "\n"mirrdump_indent"%016X   |   ", i); }
				else {}	}	}
		if(buf1 > 0x20 && buf1 < 0x7F)	{ fprintf(stderr, "%c  ", buf1); }
		else							{ fprintf(stderr, "%02X ", buf1); }
		}
	fprintf(stderr, "\n");
	}
//----------------------------------------------------------------------------------
//	fast (linear) dump
//
void fastdump(i1 *m, sz0s n) { while(n > 0) fprintf(stderr, " %02X", m[--n]);	fprintf(stderr, "\n");	};
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
