//----------------------------------------------------------------------------------
//		c.def.c
//
#ifndef C_DEF_C
#define C_DEF_C

//----------------------------------------------------------------------------------
//		globals & types
//
#include <stdint.h>

#define i0	unsigned	 int
#define i1				uint8_t
#define i2				uint16_t
#define i4				uint32_t
#define i8				uint64_t
#define i0s				 int
#define i1s				 int8_t
#define i2s				 int16_t
#define i4s				 int32_t
#define i8s				 int64_t

#define f0s				float
#define f2s				half
#define f4s				float
#define f8s				double

#define sz0				   size_t
#define sz0s			ptrdiff_t

#define     ifok    0
#define     ifno   -1

#define AL4M	__attribute__ ((aligned (4194304)))
#define AL4K	__attribute__ ((aligned (4096)))
#define AL16	__attribute__ ((aligned (16)))
#define EXCT	__attribute__ ((packed))

#include <stddef.h>
#define		ofstof		offsetof
//----------------------------------------------------------------------------------

typedef enum _BS_ {	BS_1= 0x00,			BS_2= 0x01,			BS_4= 0x02,			BS_8= 0x03,
					BS_10= 0x04,		BS_20= 0x05,		BS_40= 0x06,		BS_80= 0x07,
					BS_100= 0x08,		BS_200= 0x09,		BS_400= 0x0A,		BS_800= 0x0B,
					BS_1000= 0x0C,		BS_2000= 0x0D,		BS_4000= 0x0E,		BS_8000= 0x0F,
					BS_10000= 0x10,		BS_20000= 0x11,		BS_40000= 0x12,		BS_80000= 0x13,
					BS_100000= 0x14,	BS_200000= 0x15,	BS_400000= 0x16,	BS_800000= 0x17	} BS;
					
typedef enum _BS1_ {	BS1_00= 0x00,		BS1_01= 0x01,		BS1_02= 0x02,		BS1_04= 0x03,		
						BS1_08= 0x04,		BS1_10=	0x05,		BS1_20= 0x06,		BS1_40= 0x07	} BS1;

#define		VAL0fmV0(v0)	       v0				//	v0 as is
#define		VAL1fmV1(v1)	       v1				//	v1 as is
#define		VAL1fmV0(v0)	       v0 +1			//	convert to [1 ..10]
#define		VAL0fmV1(v1)	       v1 -1			//	convert to [0 ..F]
#define		BI04fmV0(v0)	((1 <<(v0))    >>1)
#define		BI04fmV1(v1)	((1 <<(v1 -1)) >>1)
#define		BI18fmV0(v0)	 (1 <<(v0))
#define		BI18fmV1(v1)	 (1 <<(v1 -1))
#define		MASKfmV0(v0)	((1 <<(v0 +1))  -1)		//	???
#define		MASKfmV1(v1)	((1 <<(v1))     -1)		//	???

#define		rndBLOCKS(bs, v)	((((((v &((1 <<bs) -1)) -1) >>bs) &1) ^1) +(v >>bs))
						
typedef enum _BI04toV0_ {	BI0toV0= 0x00,		BI1toV0= 0x01,		BI2toV0= 0x02,		BI4toV0= 0x03,		
							BI8toV0= 0x04,		BI10toV0= 0x05,		BI20toV0= 0x06,		BI40toV0= 0x07,
							BI80toV0= 0x08,		BI100toV0= 0x09,	BI200toV0= 0x0A,	BI400toV0= 0x0B,
							BI800toV0= 0x0C,	BI1000toV0= 0x0D,	BI2000toV0= 0x0E,	BI4000toV0= 0x0F	} BI04toV0;
//----------------------------------------------------------------------------------
//		oses, globals & headers
//
#include <errno.h>
#include <stdio.h>

#if defined(_unix) || defined(_linux) || defined(__unix) || defined(__linux)
	#define	__O__
	#define	__O_U__									//	unix family (user: "xset r off")
	#include <stdlib.h>
	#include <string.h>
	//#include <unistd.h>								//	?
	//#include <time.h>
////#include <sys/types.h>
////#include <sys/stat.h>
//	#include <sys/time.h>
//	#include <signal.h>//
	//#include <pthread.h>							//	gcc -pthread
	#if defined(__I_VI_X__)	|| defined(__XX__)		//	gcc -lX11		////	x
		#define __I_VI__
		#define __I_M__								//	mice/move
		#define __I_K__								//	keys/code
		#include "defx.c"
		#else
		#endif
	#if defined(__I_VI_F__)	|| defined(__FB__)							////	framebuffer
		#define __I_VI__
//		#include "deff.c"
		#else
		#endif
	#if defined(__I_VI_W__)	|| defined(__WL__)							////	wayland
		#define __I_VI__	
//		#include "defw.c"
		#else
		#endif
	#if defined(__I_VI_M__)	|| defined(__MR__)							////	mir
		#define __I_VI__	
//		#include "defm.c"
		#else
		#endif
	#else
	#endif

#if defined(_WIN32) || defined(_WIN64) || defined(_NT_) || defined(__WIN32) || defined(__WIN64) || defined(__NT__)
/*
*	bcc32	call to function .. with no prototype
*/
	#define	__O__
	#define	__O_W__									//	windows family
	#include <tchar.h>
	#include <windows.h>
    #define __I_VI__
    #define	__I_VI__bb				//'bb_state'
	const i0s IVI_wnd_brd_x= 6;		//'wnd_brd_x'
	const i0s IVI_wnd_brd_y= 25;	//'wnd_brd_y'
	#define __I_M__									//	mice/move
	#define __I_K__									//	keys/code
    const i0 IK_brk= 0x45;	//0x13	//'key_brk'		//	break button
	#else
	#endif

#ifndef	__O__
	#error											//	operation system is not defined
	#else
	#endif
	
#ifndef	__I_VI__bb									//	bitblt mode (0: software, 1: hardware)
	const i0s IVI_bb= 0;
	#else
	const i0s IVI_bb= 1;
	#endif
//----------------------------------------------------------------------------------

#if defined(INCLUDE_dump)
	#include "../libs/c.dump.c"
	//	void	fastdump(i1 *m, sz0s n)
	//	void	mirrordump(i1 *buf, sz0s p, sz0s sz)
	#else
	#endif
//----------------------------------------------------------------------------------

#if defined(INCLUDE_hash)
	#include "../libs/c.hash.c"
	//	void	crc4table()
	//	i4		crc4(i4 c, i1 *b, sz0s size)
	#else
	#endif
//----------------------------------------------------------------------------------

#endif